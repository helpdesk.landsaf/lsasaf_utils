Python code utilities to handle lsa-saf hdf5 data [https://landsaf.ipma.pt](https://landsaf.ipma.pt)

Contact: LSA SAF helpdesk - helpdesk.landsaf@ipma.pt 

## Required python packages:

* numpy 
* h5py
* netCDF4 

## 1. Installation

### Instalation in anaconda:
Before following the steps below for pip, it is safer to install the required packages using "conda install"
```shell
# in the conda shell (or environment)
conda install git pip netCDF4 h5py numpy
 ```

### Installation with pip:
 ```shell
 python -m pip install  git+https://gitlab.com/helpdesk.landsaf/lsasaf_utils.git
 ```
 The executable(s) will be in $HOME/.local/bin if using the system python or $CONDA_EXE/ if using anaconda

##### Installation with pip in developer mode:
Clone the repository locally and install with "-e" to allow changes in the code to take effect immediately, which is usefull for developing
 ```shell
 cd somewhere
 git clone https://gitlab.com/helpdesk.landsaf/lsasaf_utils.git
 cd lsasaf_utils
 python -m pip install -e . 
 ```
## 2. Run convGeo2Nc 
This script will convert LSA-SAF HDF5 files to netcdf4. Currently only data from MSG-Disk are supported.
```shell
convGeo2Nc -h
usage: convGeo2Nc [-h] [-e] -i FINPUT [-o FOUTPUT] [-d DSET] [-c]
                  [-b BBOX [BBOX ...]] [-r]

Info hdf5 or convert to netcdf

optional arguments:
  -h, --help          show this help message and exit
  -e                  list file contents (default: False)
  -i FINPUT           input hdf5 file (default: None)
  -o FOUTPUT          Output Netcdf file (default: None)
  -d DSET             specific dataset to convert, otherwise all datasets in
                      file (default: None)
  -c                  write lat/lon to output file (default: False)
  -b BBOX [BBOX ...]  bounding box to select: lon1 lon2 lat1 lat2 (default:
                      None)
  -r                  remap to regular 0.05x0.05 lat/lon (default: False)

# get hdf5 example file from lsa-saf page, The tool can handle compressed .bz2 
wget --no-check-certificate https://landsaf.ipma.pt/media/filer_public/6d/89/6d891c15-a256-4d40-87d4-86a813f43bae/hdf5_lsasaf_usgs_dem_msg-disk.bz2
# List file contents 
convGeo2Nc -e -i hdf5_lsasaf_usgs_dem_msg-disk.bz2
# convert raw to netcdf 
convGeo2Nc  -i hdf5_lsasaf_usgs_dem_msg-disk.bz2 -o tmp_geo_all.nc 
# convert raw to netcdf including lat/lon on output file : significant increased in file size
convGeo2Nc  -i hdf5_lsasaf_usgs_dem_msg-disk.bz2 -o tmp_geo_all_lat_lon.nc -c 
# Remap to regular grid global 
convGeo2Nc  -i hdf5_lsasaf_usgs_dem_msg-disk.bz2 -o tmp_reg_all.nc -r  
# convert raw to netcdf over a region (Iberia) and include lat/lon on output 
convGeo2Nc  -i hdf5_lsasaf_usgs_dem_msg-disk.bz2 -o tmp_geo_ib.nc -c -b -10 5 35 45
# Remap to regular grid over a region (Iberia)
convGeo2Nc  -i hdf5_lsasaf_usgs_dem_msg-disk.bz2 -o tmp_reg_ib.nc -c -b -10 5 35 45 -r 
```

## 3. Package contents
The package has two submodules and 1 executable [convGeo2Nc](#2-run-convgeo2nc)

### lsasaf_utils.geo_utils
- infer_datetime
- cut_geo
- remap_geo_near
- get_col_lin
- get_lat_lon

### lsasaf_utils.msg_utils 
- load_h5_dset
- print_header
