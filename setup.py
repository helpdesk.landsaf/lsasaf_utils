from setuptools import setup

setup(name='lsasaf_utils',
      version='1.0.0',
      description='lsasaf python utilities to handle hdf5 data ',
      url='https://gitlab.com/helpdesk.landsaf/lsasaf_utils/',
      author='Emanuel Dutra',
      author_email='endutra@gmail.com',
      license='MIT',
      packages=['lsasaf_utils'],
      zip_safe=False,
      scripts=['bin/convGeo2Nc',],
      install_requires=[
       'numpy','h5py','netCDF4','cftime',
       ]
      )
