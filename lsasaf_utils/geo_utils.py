from __future__ import print_function
import numpy as np 
import time 
import datetime as dt 

def infer_datetime(finput):
  
  try:
    slot=finput.split('_')[-1]
    if ".bz2" in slot:
      slot=slot[:-4]
    ddate=dt.datetime.strptime(slot,"%Y%m%d%H%M") 
  except:
    print("Could not infer datetime from input file %s"%finput)
    ddate=None
  return ddate

def cut_geo(xdata,bbox,verbose=False):
  
  t0 = time.time()
  
  x1,y1=get_col_lin(bbox[2],bbox[0])
  x2,y2=get_col_lin(bbox[3],bbox[1])
  if x1 > x2: x1,x2 = x2,x1
  if y1 > y2: y1,y2 = y2,y1  
  print('Cutting domain to:')
  print(x1,y1,get_lat_lon(x1,y1))
  print(x2,y2,get_lat_lon(x2,y2))
  # Cut variables
  xdata_out={}
  for cv in list(xdata.keys()):
   xdata_out[cv]=xdata[cv][y1:y2+1,x1:x2+1]
  
  xcol,xlin=np.meshgrid(range(x1,x2+1),range(y1,y2+1))
  llon,llat = get_lat_lon(xcol,xlin)
  if verbose:
    print('Finished cutting in %3.2f seconds:'%(time.time()-t0))
  
  return xdata_out,llon,llat
 
def remap_geo_near(xdata,xattr,rlon,rlat,bbox=None,verbose=False):
  
  t0 = time.time()

  if bbox is not None:
    rlon_out=rlon[ (rlon >= bbox[0]) & (rlon <= bbox[1])]
    rlat_out=rlat[ (rlat >= bbox[2]) & (rlat <= bbox[3])]
  else:
    rlon_out,rlat_out=rlon,rlat 
    
  rllon,rllat= np.meshgrid(rlon_out,rlat_out)
  xcol,xlin=get_col_lin(rllat,rllon)
  
  # reshape data
  xmiss=(xlin==-1) | (xcol==-1)
  xdata_out={}
  for cv in list(xdata.keys()):
     xdata_out[cv]=xdata[cv][xlin,xcol]
     if xattr[cv]['MISSING_VALUE'] is not None: xdata_out[cv][xmiss]=xattr[cv]['MISSING_VALUE']
  if verbose:
    print("Reshaping to regular grid in %3.2f seconds"%(time.time()-t0))
  return xdata_out,rlon_out,rlat_out 

def get_col_lin(lat, lon, region='MSG-Disk'):

    CFAC=13642337
    LFAC=13642337
    sub_lon=0
    if region in 'MSG-Disk':
        COFF=1857
        LOFF=1857
        iarea = 4
    elif region in 'Euro':
        COFF=308
        LOFF=1808
        iarea = 0
    elif region in 'NAfr':
        COFF=618
        LOFF=1158
        iarea = 1
    elif region in 'SAfr':
        COFF=-282
        LOFF=8
        iarea = 2
    elif region in 'SAme':
        COFF=1818
        LOFF=398
        iarea = 3
    elif region in 'CoMd':
        COFF = 645
        LOFF = 1475
        iarea = 5
    elif region in 'HMWR-Disk':
        CFAC=20466275
        LFAC=20466275
        LOFF=2751
        COFF=2751
        sub_lon=140.70
        iarea=6
        
    else:
        print('Unknown region!!!')
        return None, None
    
    AUX16 = 2**-16
    p1 = 42164.
    p2 = 0.00675701
    p3 = 0.993243
    rp = 6356.5838

    # Projection
    c_lat = np.degrees(np.arctan(p3 * np.tan(np.radians(lat))))
    rl    = rp / np.sqrt(1. - p2 * (np.cos(np.radians(c_lat)))**2)
    r1    = p1 - rl * np.cos(np.radians(c_lat)) * np.cos(np.radians(lon - sub_lon))
    r2    = -rl * np.cos(np.radians(c_lat)) * np.sin(np.radians(lon - sub_lon))
    r3    = rl * np.sin(np.radians(c_lat))
    rn    = np.sqrt(r1**2 + r2**2 + r3**2)

    x     = np.degrees(np.arctan(-r2/r1))
    y     = np.degrees(np.arcsin(-r3/rn))

    # Scaling function
    col   = COFF + np.round(x*AUX16*CFAC).astype(np.int16) - 1 
    lin   = LOFF + np.round(y*AUX16*LFAC).astype(np.int16) - 1
    
    # mask values outside of disk 
    ## assuming a shere (E. Dutra) not sure it's the most correct approach, use limit of 78 instead of 80, just to be sure  
    angle = np.rad2deg(np.arccos(np.cos(np.deg2rad(lat))*np.cos(np.deg2rad(lon))))
    if isinstance(col,np.ndarray):
      col[angle>78]=-1
      lin[angle>78]=-1
    else:
      if angle > 78:
        col=-1
        lin=-1
        
    return col,lin

def get_lat_lon(col,lin,region='MSG-Disk'):
    
    
    miss_value = -999.

    AUX16 = 2.**(-16)
    p1 = 42164.
    p2 = 1.006803
    p3 = 1737121856
    CFAC = 13642337
    LFAC = 13642337
    sub_lon = 0
    
    if region == 'HMWR-Disk':
        CFAC = 20466275
        LFAC = 20466275
        COFF = 2750
        LOFF = 2750
        sub_lon = 140.70
    elif region in 'MSG-Disk':
        COFF=1857
        LOFF=1857
        iarea = 4
    elif region in 'Euro':
        COFF=308
        LOFF=1808
        iarea = 0
    elif region in 'NAfr':
        COFF=618
        LOFF=1158
        iarea = 1
    elif region in 'SAfr':
        COFF=-282
        LOFF=8
        iarea = 2
    elif region in 'SAme':
        COFF=1818
        LOFF=398
        iarea = 3
    elif region in 'CoMd':
        COFF = 645
        LOFF = 1475
        iarea = 5

    x = (col+1-COFF)/(AUX16*CFAC)  # Degrees
    x = x*np.pi/180.			 # Radians
    y = (lin+1-LOFF)/(AUX16*LFAC)  # Degrees
    y = y*np.pi/180.			 # Radians

    fac = (p1 * np.cos(x) * np.cos(y))**2 - (np.cos(y)**2 + p2 * np.sin(y)**2) * p3
    
    # process entire ndarray
    if isinstance(col,np.ndarray):
        lat = np.zeros(np.shape(col))
        lon = np.zeros(np.shape(col))

        ind = fac<0    
        lon[ind] = miss_value
        lat[ind] = miss_value
        
        ind = fac>=0
        sd = np.sqrt((p1 * np.cos(x[ind]) * np.cos(y[ind]))**2 - (np.cos(y[ind])**2 + p2 * np.sin(y[ind])**2) * p3)
        sn = (p1 * np.cos(x[ind]) *np.cos(y[ind]) -sd)/(np.cos(y[ind])**2 + p2 * np.sin(y[ind])**2)
        s1 = p1 - sn * np.cos(x[ind]) * np.cos(y[ind])
        s2 = sn * np.sin(x[ind]) * np.cos(y[ind])
        s3 = -sn * np.sin(y[ind])
        sxy = np.sqrt(s1**2 + s2**2)

        lon[ind] = np.arctan((s2/s1)) 
        lon[ind] =lon[ind]*180/np.pi + sub_lon
        lon[lon>180.] = lon[lon>180.]-360.
        lat[ind] = np.arctan(p2 * (s3/sxy))
        lat[ind]=lat[ind]*180./np.pi
        
        return lon,lat
    #process single value (station, single pixel)
    else:
        if fac<0:
            lon = miss_value
            lat = miss_value
        else:
            
            sd = np.sqrt((p1 * np.cos(x) * np.cos(y))**2 - (np.cos(y)**2 + p2 * np.sin(y)**2) * p3)
            sn = (p1 * np.cos(x) *np.cos(y) -sd)/(np.cos(y)**2 + p2 * np.sin(y)**2)
            s1 = p1 - sn * np.cos(x) * np.cos(y)
            s2 = sn * np.sin(x) * np.cos(y)
            s3 = -sn * np.sin(y)
            sxy = np.sqrt(s1**2 + s2**2)

            lon = np.arctan((s2/s1)) 
            lon = lon*180/np.pi + sub_lon
            if lon>180.:
                lon = lon-360.
            lat = np.arctan(p2 * (s3/sxy))
            lat=lat*180./np.pi
        
        return lon,lat 
