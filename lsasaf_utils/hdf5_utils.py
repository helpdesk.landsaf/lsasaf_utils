from __future__ import print_function
import numpy as np 
import h5py
import time
import bz2
import os

def load_h5_dset(ffile,cvar=None,ddtype=None,verbose=False,zmiss=None,mask=None):
  
  t0=time.time()
  
  # find if it's a bz2 file 
  if '.bz2' in ffile:
    LBZ2=True
  else:
    LBZ2=False
    
  # unzip and open file 
  if LBZ2:
    ff = bz2.BZ2File(ffile)
  else:
    ff = ffile
  h5 = h5py.File(ff,'r')
  cvarI = cvar 
  # find dataset names in file 
  if cvar is None:
    cvars=list_h5_dsets(h5)
  else:
    if type(cvar) is list :
      cvars = cvar 
    else:
      cvars=[cvar]
    for cv in cvars:
      if type(h5[cv]) is not h5py._hl.dataset.Dataset:
        print(cv,list(h5.keys()))
        raise NameError("Provided dataset name not in file")
    
 
    
  xout={} # dic to store data arrays
  attr={} # dic to store data attributes
  # loop on datasets
  for cvar in cvars:
    
    #initilaize attributes to default values 
    attr[cvar]= {'UNITS':'-',
                 'PRODUCT':cvar,
                 'SCALING_FACTOR':1,
                 'MISSING_VALUE':None,
                 'OFFSET':0.,
                 'dtype':'f4'}
    # read atts from dataset
    for kk in attr[cvar].keys():
      try:
        attr[cvar][kk] = h5[cvar].attrs[kk]
      except:
        pass
    
    # Load data 
    xvar = h5[cvar][:,:]
    attr[cvar]['dtype']=xvar.dtype
    
    # cut data to mask, if provided 
    if mask is not None:
      xvar= xvar[mask]
    
    if zmiss is not None:
      ## apply scaling and add offset   
      xvar=attr[cvar]['OFFSET'] + xvar.astype(ddtype) / attr[cvar]['SCALING_FACTOR']
    
      ## Mask if missing value was defined 
      if attr[cvar]['MISSING_VALUE'] is not None:
        xvar[xvar == attr[cvar]['OFFSET']+ attr[cvar]['MISSING_VALUE']/attr[cvar]['SCALING_FACTOR']]=zmiss
    
    ## Add to output dic  
    xout[cvar]=xvar 
    if verbose:
      print(attr[cvar])
      #print('')
  
  #close 
  h5.close()
  if LBZ2:
    ff.close()
  if verbose:
    print('Reading %s from %s in %4.2f sec'%(' '.join(cvars),ffile,(time.time()-t0)))
  if type(cvarI) is not list and cvarI is not None:
    return xout[cvarI],attr[cvarI]
  else:
    return xout,attr

def list_h5_dsets(f):
  dsets=[]
  for k in f.keys():
    if type(f[k]) is h5py._hl.dataset.Dataset:
      dsets.append(k)
    elif type(f[k]) is h5py._hl.group.Group:
      for kk in f[k].keys():
        kk1=k+"/"+kk
        if type(f[kk1]) is h5py._hl.dataset.Dataset:
          dsets.append(kk1)
  return dsets

def print_header(ffile):
   
  if '.bz2' in ffile:
    LBZ2=True
  else:
    LBZ2=False
    
  if LBZ2:
    ff = bz2.BZ2File(ffile)
  else:
    ff = ffile
  f = h5py.File(ff,'r')
  print("########################")
  print("##conv2NC:print_header##")
  print("FILE:",ffile)
  #get global attributes of the hdf5 file
  for attr in f.attrs.keys():
      print("global attribute %s : %s" % (attr,f.attrs[attr]))

  #list the hdf5 datasets. It opens like a python dictionary
  print("keys: \n%s" % f.keys())
  dsets=list_h5_dsets(f)
  print("Dsets: \n%s",dsets)
  #get dataset shape, datatype, dataset attributes and the dataset itself
  for key in dsets:
      dset= f[key]
      print("Dataset %s shape: %s" % (key,dset.shape))
      print("Dataset %s dtype: %s" % (key,dset.dtype))
      for attr in dset.attrs.keys():
        try:
          print("Dataset %s attr %s : %s" % (key,attr,dset.attrs[attr]))
        except:
          pass
      print(dset)
      xdata,xatt=load_h5_dset(ffile,cvar=key,ddtype='f4',verbose=False,zmiss=np.nan,mask=None)
      print("#missing, min, mean,max:",np.sum(np.isnan(xdata)),
            np.nanmin(xdata),np.nanmean(xdata),np.nanmax(xdata))
  f.close()
  if LBZ2:
    ff.close()
